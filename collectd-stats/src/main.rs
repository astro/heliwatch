use std::time::Duration;
use tokio::time::sleep;
use nav_types::WGS84;

const INTERVAL: u64 = 10;

fn update_min<T: Clone + PartialOrd>(value: &Option<T>, target: &mut Option<T>) {
    match (value, target) {
        (Some(value), target) if target.is_none() =>
            *target = Some(value.clone()),
        (Some(value), Some(target)) if value < target =>
            *target = value.clone(),
        _ => {}
    };
}

fn update_max<T: Clone + PartialOrd>(value: &Option<T>, target: &mut Option<T>) {
    match (value, target) {
        (Some(value), target) if target.is_none() =>
            *target = Some(value.clone()),
        (Some(value), Some(target)) if value > target =>
            *target = value.clone(),
        _ => {}
    };
}

#[tokio::main]
async fn main() {
    let orig_hook = std::panic::take_hook();
    std::panic::set_hook(Box::new(move |panic_info| {
        // invoke the default handler and exit the process
        orig_hook(panic_info);
        std::process::exit(1);
    }));

    let home = WGS84::from_degrees_and_meters(51.081, 13.728, 0.0);

    let aircrafts = beast::aircrafts::Aircrafts::new();
    aircrafts.connect("radiobert.serv.zentralwerk.org", 30005);
    let hostname_line = std::fs::read_to_string("/proc/sys/kernel/hostname")
        .unwrap();
    let hostname = hostname_line.split(char::is_whitespace)
        .next()
        .unwrap();

    loop {
        sleep(Duration::from_secs(INTERVAL)).await;

        let mut count = 0;
        let mut min_altitude = None;
        let mut max_altitude = None;
        let mut min_speed = None;
        let mut max_speed = None;
        let mut min_distance = None;
        let mut max_distance = None;
        let aircrafts = aircrafts.read();
        for (_, entry) in aircrafts.iter() {
            let entry = entry.read().unwrap();
            count += 1;

            update_min(&entry.altitude, &mut min_altitude);
            update_max(&entry.altitude, &mut max_altitude);

            update_min(&entry.speed, &mut min_speed);
            update_max(&entry.speed, &mut max_speed);

            if let Some(pos) = entry.position() {
                let altitude_m = entry.altitude.unwrap_or(0) as f64 * 0.3048;
                let distance_km = WGS84::from_degrees_and_meters(pos.latitude, pos.longitude, altitude_m)
                    .distance(&home) / 1000.0;
                update_min(&Some(distance_km), &mut min_distance);
                update_max(&Some(distance_km), &mut max_distance);
            }
        }
        println!("PUTVAL \"{}/beast-aircrafts/records\" interval={} N:{}", hostname, INTERVAL, count);

        if let Some(min_altitude) = min_altitude { println!("PUTVAL \"{}/beast-altitude/current-min\" interval={} N:{}", hostname, INTERVAL, min_altitude) }
        if let Some(max_altitude) = max_altitude { println!("PUTVAL \"{}/beast-altitude/current-max\" interval={} N:{}", hostname, INTERVAL, max_altitude) }

        if let Some(min_speed) = min_speed { println!("PUTVAL \"{}/beast-speed/current-min\" interval={} N:{:.3}", hostname, INTERVAL, min_speed) }
        if let Some(max_speed) = max_speed { println!("PUTVAL \"{}/beast-speed/current-max\" interval={} N:{:.3}", hostname, INTERVAL, max_speed) }

        if let Some(min_distance) = min_distance { println!("PUTVAL \"{}/beast-distance/current-min\" interval={} N:{:.3}", hostname, INTERVAL, min_distance) }
        if let Some(max_distance) = max_distance { println!("PUTVAL \"{}/beast-distance/current-max\" interval={} N:{:.3}", hostname, INTERVAL, max_distance) }
    }
}
