{ self }:
{ config, lib, pkgs, ... }: {
  options.services.heliwatch = with lib; {
    enable = mkEnableOption "Enable Heliwatch MUC bot";
    jid = mkOption {
      type = types.str;
    };
    passwordFile = mkOption {
      type = types.str;
    };
    muc = mkOption {
      type = types.str;
      description = "Full Jabber-Id in the form of room@service/nickname";
    };
    user = mkOption {
      type = types.str;
      default = "heliwatch";
    };
    group = mkOption {
      type = types.str;
      default = "heliwatch";
    };
  };

  config =
    let
      cfg = config.services.heliwatch;
    in
      lib.mkIf cfg.enable {
        users = {
          users.${cfg.user} = {
            isSystemUser = true;
            home = "/home/heliwatch";
            createHome = true;
            group = cfg.group;
          };
          groups.${cfg.group} = {};
        };

        systemd.services.heliwatch = {
          wantedBy = [ "multi-user.target" ];
          after = [ "network.target" ];
          path = with pkgs; [ bash curl ];
          script = with cfg; ''
            ${./fetch_data.sh}
            exec ${lib.getExe self.packages.${pkgs.stdenv.system}.heliwatch} '${jid}' "$(cat ${passwordFile})" '${muc}'
          '';
          serviceConfig = {
            Type = "simple";
            User = cfg.user;
            Group = cfg.group;
            WorkingDirectory = config.users.users.${cfg.user}.home;
            ReadWritePaths = config.users.users.${cfg.user}.home;
            ProtectSystem = "full";
            Restart = "always";
            RestartSec = "60s";
          };
        };
      };
}
