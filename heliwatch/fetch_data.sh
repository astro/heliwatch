#!/usr/bin/env sh

set -e

overpass() {
    curl -X POST -d "data=[out:json];$1" -o $2 http://overpass-api.de/api/interpreter
}

bbox="51,13,52,15"
Q=""
for level in 6 7 8 9 10 11 ; do
    Q=$Q'way["boundary"="administrative"]["admin_level"="'$level'"]('$bbox'); relation["boundary"="administrative"]["admin_level"="'$level'"]('$bbox');'
done

if [ ! -e locations.json ]; then
    F=$(mktemp)
    overpass "($Q); out body; >; out skel;" $F
    mv $F locations.json
fi

if [ ! -e aircraftDatabase.csv ]; then
    F=$(mktemp)
    curl https://opensky-network.org/datasets/metadata/aircraftDatabase.csv -o $F
    mv $F aircraftDatabase.csv
fi
