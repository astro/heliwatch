{
  inputs = {
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nmattia/naersk";
    naersk.inputs.nixpkgs.follows = "nixpkgs";
    fenix.url = "github:nix-community/fenix";
    fenix.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, utils, fenix, naersk }:
    utils.lib.eachSystem (with utils.lib.system; [ x86_64-linux aarch64-linux ]) (system: let
      pkgs = nixpkgs.legacyPackages."${system}";

      rust = fenix.packages.${system}.stable.withComponents [
        "cargo"
        "rustc"
        "rust-src"  # just for rust-analyzer
        "clippy"
      ];

      # Override the version used in naersk
      naersk-lib = naersk.lib."${system}".override {
        cargo = rust;
        rustc = rust;
      };

      buildRustPackage = args: naersk-lib.buildPackage ({
        name = args.pname;
        src = ./.;
        targets = [ args.pname ];
        cargoBuildOptions = opts: opts ++ [ "-p" args.pname ];
        nativeBuildInputs = with pkgs; [ pkg-config ];
        buildInputs = with pkgs; [ openssl ];
        meta.mainProgram = args.pname;
      } // args);
    in rec {
      # `nix build`
      packages.heliwatch = buildRustPackage {
        pname = "heliwatch";
      };
      packages.http-json = buildRustPackage {
        pname = "http-json";
      };
      packages.collectd-stats = buildRustPackage {
        pname = "collectd-stats";
      };
      defaultPackage = packages.heliwatch;

      checks = packages;

      # `nix run`
      apps.heliwatch = utils.lib.mkApp {
        drv = packages.heliwatch;
      };
      defaultApp = apps.heliwatch;

      # `nix develop`
      devShell = pkgs.mkShell {
        nativeBuildInputs = [
          rust
          fenix.packages.${system}.rust-analyzer
        ] ++ (with defaultPackage;
          nativeBuildInputs ++ buildInputs
        );
      };
    }) // {
      overlay = final: prev: {
        heliwatch = self.packages.${prev.stdenv.system};
      };

      nixosModules.heliwatch = import ./heliwatch/module.nix { inherit self; };
    };
}
